const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
const path = require('path');
const usersRoutes = require('./routes/users-routes');

mongoose.connect('mongodb+srv://raph-admin:ih2a4rFVetWEPmzw@cluster0.7wzf7.mongodb.net/consigne?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(
    () => console.log('Connexion à MongoDB réussie !')
).catch(
    () => console.log('Connexion à MongoDB échouée !')
);

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(bodyParser.json());

app.use('/images', express.static(path.join(__dirname, 'images')));
app.use('/users', usersRoutes);
app.use('/auth', usersRoutes);

module.exports = app;
