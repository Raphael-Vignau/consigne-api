const jwt = require('jsonwebtoken');
const tokenConfig = require('../config/token')

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, tokenConfig.RANDOM_SECRET_KEY);
        const idUser = decodedToken.idUser;
        if (req.body.idUser && req.body.idUser !== idUser) {
            throw 'Invalid user ID'
        } else {
            next();
        }
    } catch {
        res.status(401).json({
            error: new Error('Invalid request !')
        });
    }
};
