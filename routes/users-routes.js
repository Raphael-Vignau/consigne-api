const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/users-ctrl');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');

router.get('/', auth, userCtrl.getUsers);
router.get('/count', auth, userCtrl.countUsers);
router.get('/:id', auth, userCtrl.getOneUser);

router.post('/', auth, multer, userCtrl.createUser);
router.post('/signup', userCtrl.signup);
router.post('/login', userCtrl.login);

router.put('/:id', auth, multer, userCtrl.editUser);
router.delete('/:id', auth, userCtrl.deleteUser);

module.exports = router;
