const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const tokenConfig = require('../config/token')

exports.signup = (req, res) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            const user = new User({
                email: req.body.email,
                username: req.body.username,
                password: hash
            });
            //Todo retourner l'utilisateur complet avec son tocken pour le connecter directement
            user.save()
                .then(userCreated => res.status(201).json({username: userCreated.username}))
                .catch(error => res.status(400).json({error}))
        })
        .catch(error => res.status(500).json({error}));
}


exports.login = (req, res) => {
    User.findOne({$or: [{email: req.body.identifier}, {username: req.body.identifier}]})
        .then(user => {
            if (!user) {
                return res.status(401).json({message: 'No founded'})
            }
            bcrypt.compare(req.body.password, user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({message: 'Wrong password'})
                    }
                    res.status(200).json({
                        id: user._id,
                        username: user.username,
                        email: user.email,
                        token: jwt.sign(
                            {idUser: user._id},
                            tokenConfig.RANDOM_SECRET_KEY,
                            {expiresIn: tokenConfig.expiration}
                        )
                    })
                })
                .catch(error => res.status(500).json({error}))
        })
        .catch(error => res.status(500).json({error}))
}

exports.createUser = (req, res) => {
    bcrypt.hash(req.file ? req.body.user.password : req.body.password, 10)
        .then(hash => {
            const userObject = req.file ?
                {
                    ...JSON.parse(req.body.user),
                    imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
                } : {...req.body};
            delete userObject._id;
            userObject.password = hash;
            const user = new User({
                ...userObject
            });
            user.save()
                .then(user => res.status(201).json(user))
                .catch(error => res.status(400).json({error}))
        })
        .catch(error => res.status(500).json({error}));
}

exports.editUser = (req, res) => {
    const userObject = req.file ?
        {
            ...JSON.parse(req.body.user),
            imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
        } : {...req.body};
    User.updateOne({_id: req.params.id}, {...userObject, _id: req.params.id})
        .then(() => res.status(200).json({message: 'User edited !'}))
        .catch(error => res.status(400).json({error}));
}

exports.deleteUser = (req, res) => {
    User.deleteOne({_id: req.params.id})
        .then(() => res.status(200).json({message: 'User delete !'}))
        .catch(error => res.status(400).json({error}));
}

exports.countUsers = (req, res) => {
    User.countDocuments()
        .then(numberUsers => res.status(200).json(numberUsers))
        .catch(error => res.status(400).json({error}));
}

exports.getOneUser = (req, res) => {
    User.findOne({_id: req.params.id})
        .then(user => res.status(200).json(user))
        .catch(error => res.status(400).json({error}));
}

exports.getUsers = (req, res) => {
    User.find().skip(parseInt(req.query._start)).limit(parseInt(req.query._limit))
        .then(users => res.status(200).json(users))
        .catch(error => res.status(400).json({error}));
}
